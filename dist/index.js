"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Environment Variables
const dotenv_1 = __importDefault(require("dotenv"));
// Server
const server_1 = __importDefault(require("./src/server"));
const logger_1 = require("./src/utils/logger");
// Configuration the .env file
dotenv_1.default.config();
// Port
const port = process.env.PORT || 8000;
// Execute APP and Listen Request to PORT
server_1.default.listen(port, () => {
    (0, logger_1.LogSuccess)(`[SERVER ON]: Running in http://localhost:${port}/api`);
});
// Control SERVER ERROR
server_1.default.on('error', (error) => {
    (0, logger_1.LogError)(`[SERVER ERROR]: ${error}`);
});
//# sourceMappingURL=index.js.map