// Environment Variables
import dotenv from 'dotenv';

// Server
import server from './src/server';
import { LogSuccess, LogError } from './src/utils/logger';

// Configuration the .env file
dotenv.config();

// Port
const port = process.env.PORT || 8000;

// Execute APP and Listen Request to PORT
server.listen(port,()=>{
    LogSuccess(`[SERVER ON]: Running in http://localhost:${port}/api`)
});

// Control SERVER ERROR
server.on('error',(error)=>{
    LogError(`[SERVER ERROR]: ${error}`);
})