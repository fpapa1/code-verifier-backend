import { IGoodbyeController } from './interfaces/index';
import { GoodbyeResponse } from './types';
import { LogSuccess } from '../utils/logger';

export class GoodbyeController implements IGoodbyeController{
    public async getMessage(name?: string, date?: string): Promise<GoodbyeResponse> {
        LogSuccess('[/api/goodbye] Get Request');
        return{
            message: `Hello ${name || 'World'} on ${date || Date()}`
        }
    }

}