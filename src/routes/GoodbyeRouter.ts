import express, { Request, Response } from "express";
import { GoodbyeController } from "../controller/GoodbyeController";
import { LogInfo } from '../utils/logger';

// Router from express
let goodbyeRouter = express.Router();

// http://localhost:8000/api/goodbye?name=Fredy&date=24012023/
goodbyeRouter.route('/')
    //GET:
    .get(async(req:Request,res:Response)=>{
        // Obtain a Query Param
        let name:any = req?.query?.name;
        let date:any = req?.query?.date;
        LogInfo(`Query Param name: ${name}`);
        LogInfo(`Query Param date: ${date}`);
        // Controller Instance to execute method
        const controller:GoodbyeController = new GoodbyeController();
        // Obtain Responde
        const response = await controller.getMessage(name,date);
        // Send to the client the response
        return res.send(response);
    });

    //Export Hello Router
    export default goodbyeRouter;